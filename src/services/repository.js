import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

export default class Repository {
  constructor () {
    this.baseUrl = 'https://blogapp-f47c.restdb.io/rest'
    this.options = {
      headers: {
        'cache-control': 'no-cache',
        'x-apikey': '5a9c6cd6c2c2549e6910fdaa',
        'content-type': 'application/json'
      }
    }
  }

  createArticle (article) {
    return Vue.http.post(this.baseUrl + '/articles', article, this.options)
      .then(function (data) {
        return data.json()
      })
  }

  updateArticle (id, article) {
    return Vue.http.put(this.baseUrl + '/articles/' + id, article, this.options)
      .then(function (data) {
        return data.json()
      })
  }

  deleteArticle (id) {
    return Vue.http.delete(this.baseUrl + '/articles/' + id, this.options)
      .then(function (data) {
        return data.json()
      })
  }

  getArticles () {
    return Vue.http.get(this.baseUrl + '/articles', this.options)
      .then(function (data) {
        return data.json()
      })
      .then(function (data) {
        return data.sort((lastOne, nextOne) => {
          return lastOne._id > nextOne._id ? 1 : -1
        })
      })
  }

  getArticle (id) {
    return Vue.http.get(this.baseUrl + '/articles/' + id, this.options)
      .then(function (data) {
        return data.json()
      })
      .then(function (data) {
        return {
          title: data.title,
          content: data.content,
          published: data.published
        }
      })
  }
}
