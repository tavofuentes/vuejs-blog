export default {
  filters: {
    'trim': function (value, maxLength) {
      return value.slice(0, maxLength || 100) + ' ...'
    }
  }
}
