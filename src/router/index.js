import Vue from 'vue'
import Router from 'vue-router'
import listArticles from '@/components/listArticles.vue'
import dashboard from '@/components/dashboard.vue'
import addArticle from '@/components/addArticle.vue'
import viewArticle from '@/components/viewArticle.vue'
import editArticle from '@/components/editArticle.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: listArticles
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: dashboard
    },
    {
      path: '/article/new',
      name: 'createArticle',
      component: addArticle
    },
    {
      path: '/article/:id',
      name: 'viewArticle',
      component: viewArticle
    },
    {
      path: '/article/:id/edit',
      name: 'editArticle',
      component: editArticle
    }
  ]
})
