# vuejs-blog

> Article publishing platform

Built with vuejs and styled with Bootstrap4 (only CSS).

For prototyping purposes it is connected to a NoSQL database ([restdb.io](https://restdb.io)) and deployed to heroku as a [demo here](https://ofa-vuejs-blog.herokuapp.com).

## Build Setup

``` bash
# install dependencies
npm|yarn install

# serve with hot reload at localhost:8080
npm|yarn run dev

# build for production with minification
npm|yarn run build
```
